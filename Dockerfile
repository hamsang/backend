FROM python:3-alpine
COPY . /hamsang
WORKDIR /hamsang/api
RUN pip install uvicorn fastapi
CMD uvicorn main:app --reload --port 80 --host 0.0.0.0
