from typing import Optional
from fastapi import FastAPI, Response
from core.dbmanager import get_meaning
import core.dbmanager as dbmanager
from fastapi.middleware.cors import CORSMiddleware
import json

app = FastAPI()

origins = [
    "http://localhost.tiangolo.com",
    "https://localhost.tiangolo.com",
    "http://localhost",
    "http://localhost:8000",
    "https://localhost:8000",
    "*"
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    # allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)



@app.get("/")
def read_root():
    result = {"Hello": "World"}

    headers = {
        "Origin":"*",
        "access-control-allow-origin": "*",
    }

    res = Response(content=result.__repr__(), headers=headers)

    return res



@app.get("/api/v0/word/{word}")
def get_term_from_all_dic(word: str):
    output = get_meaning(word, '*')

    result = {
        "success": "true",
        "message": "Here is what you want",
        "data": output
    }

    headers = {
        "Origin":"*",
        "access-control-allow-origin": "*",
        "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE",
        "Access-Control-Allow-Headers": "Authorization",
        "content-type": "application/json",
        "charset":"UTF-8",

    }

    res = Response(content=json.dumps(result), headers=headers)

    return res



@app.get("/api/v0/dicts")
def get_dicts_info():
    output = dbmanager.get_dicts_info()

    result = {
        "success": "true",
        "message": "Here is what you want",
        "data": output
    }

    headers = {
        "Origin":"*",
        "access-control-allow-origin": "*",
        "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE",
        "Access-Control-Allow-Headers": "Authorization",
        "content-type": "application/json",
        "charset":"UTF-8",

    }

    res = Response(content=json.dumps(result), headers=headers)

    return res

