## how to run

### using fastapi


#### using docker
```bash
docker build -t hamsang .
docker run -p 80:80 hamsang
```

#### manually

```bash
pip install fastapi uvicorn

cd api
uvicorn main:app --reload
```

### to run using flask (old)
```bash
cd api
flask run
```
